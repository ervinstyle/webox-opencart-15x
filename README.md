# Webox Csomagautomata telepítése OpenCart 1.5.x

## `FONTOS!!!` Készíts biztonsági másolatot rendszeredről.

Töltse fel a fájlokat mappával a megfelelő helyre, FTP kapcsolaton keresztül a tárhelyére. 
Az alábbi SQL utasítást futtasd az adatbázison (PhpMyAdmin felületen). Ha prefix-et használ a rendszer akkor az `order` elé kell tenni a megfelelő prefixet. pl.: `oc_order`

```sql
ALTER TABLE `order` ADD `webox_mashine` VARCHAR( 50 ) NOT NULL AFTER `shipping_code` ;
```
Amennyiben vQmod-ot használsz nem kell a fájlokat módosítani, azokat a mellékelt vQmod XML elvégzi.
Módosítsd az alábbi fájlokat:`admin/language/hungarian/common/header.php` fájl végére illeszd be (ha szeretnéd más nyelveken is elérni az admin felületen akkor minden nyelv `admin/language/*/common/header.php` fájlba be kell illeszteni)

```
     …
     $_['text_webox_order']           = 'Webox csomag';
```
`admin/controller/common/header.php` fájl kiegészítése az alábbiak szeint
```
     …
     $this->data['text_webox_order'] = $this->language->get('text_webox_order');
     …
     $this->data['webox_order'] = $this->url->link('sale/webox_order', 'token=' . $this->session->data['token'], 'SSL');
     …
```

`admin/view/template/common/header.tpl` fájl kiegészítése az alábbiak szeint
```
     …
     <li><a href="<?php echo $webox; ?>"><?php echo $text_webox_order	; ?></a></li>
```

`catalog/view/theme/default/template/checkout/shipping_method.tpl` fájl módosítása. Az alábbi sorokat kell a fájl végére tenni (Ha a sablon amit használsz más mappában van akkor az abban található fájlt kell módosítani)
```
<?php echo $quote['title']; ?></label>
		<?php if ($quote['code']=='webox.webox'){ ?>
		<?php  $webox_plase_select = $quote['error_mashine_selected']; ?>
		<?php  echo $quote['text_description']; ?>		
       <select id="webox_machine" name="webox_machine" onchange="EPWidget.updateFromDropdown(this)"></select>
        <a href="#" onclick="openMap('MAP-radio-all-machines'); return false;"><?php echo $quote['text_map']; ?></a>
<?php } ?>
        …
//Ezt a fájl végére
        <script type="text/javascript">
            function map_callback( value ) { 
                console.log(value); 
                var address = value.split(';'); 
                var box_machine_name = document.value=address[0]; 
                var box_machine_town = document.value=address[1]; 
                var box_machine_street = document.value=address[2]; 

                var is_value = 0; 
                document.getElementById('webox.webox').checked = true; 
                 
            } 
          
            $.getScript("http://geowidget-hu.easypack24.net/dropdown.php?dropdown_name=webox_machine&user_function=map_callback").done(function () 
            { 
                document.getElementById('webox_machine').innerHTML = EPWidget.getOptions(); 
                document.getElementById('webox_machine').firstChild.innerHTML = '<?php echo $webox_plase_select; ?>'; 
                 
            }); 
        </script>
```
`catalog/view/theme/default/template/checkout/checkout.tpl` fájl módosítása. Az alábbi sort kell kicserélni a (Ha a sablon amit használsz más mappában van akkor az abban található fájlt kell módosítani)
        ezt:
```
        data: $('#shipping-method input[type=\'radio\']:checked, #shipping-method textarea'),
```
erre:
```
        data: $('#shipping-method input[type=\'radio\']:checked, #shipping-method textarea, #shipping-method select, #shipping-method input[type=\'text\']'),
```
`catalog/controller/checkout/shipping_method.php` fájl végén a `$this->response->setOutput(json_encode($json));` sor elé illeszd be a következőt
```
        ...
        if ($this->request->post['shipping_method']=="webox.webox" && (!$this->request->post['webox_machine'] || $this->request->post['webox_machine']=='') )
                    {
                        $this->language->load('shipping/webox');
                        $json['error']['warning'] = $this->language->get('error_mashine_selected');
                    }
                    else
                    {
                        $this->session->data['shipping_webox_mashine'] = $this->request->post['webox_machine'];
                    }
                    
                    if ($this->request->post['shipping_method']!="webox.webox")
                    {
                        $this->session->data['shipping_webox_mashine'] = '';
                        unset($this->session->data['shipping_webox_mashine']);
                    }
```
        		

`catalog/controller/checkout/confirm.php` fájl kiegészítése az alábbiak szeint közvetlenül a ` $this->session->data['order_id'] = $this->model_checkout_order->addOrder($data);` sor után
```        ...
        if (isset($data['shipping_code']) == "webox.webox") {
           $this->db->query("UPDATE `".DB_PREFIX."order` SET webox_mashine='".$this->session->data['shipping_webox_mashine']."' WHERE order_id=".$this->session->data['order_id']);
        }
```
Lépj be áruházad admin felületére és Bővítmények -> Szállítási módok menüpont alatt megtalálod a Webox Csomagautomata (0-24) modult. [Bekapcsolás] után [Módosítás] és itt beállíthatod a szállítási díjat, sorrendet illetve a stáruszt is.
    A megrendeléseket, amelyeket Webox Csomagautomatával rendeltek, megtalálod az Eladások -> Webox csomag menüpont alatt.