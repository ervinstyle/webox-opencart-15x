<?php
// Címsor
$_['heading_title']    = 'Webox';

// Szövegek
$_['text_shipping']    = 'Webox';
$_['text_success']     = 'A Webox modul módosítása sikeresen megtörtént!';

// Bejegyzések
$_['entry_cost']       = 'Szállítási költség:';
$_['entry_tax_class']  = 'Adóosztály:';
$_['entry_geo_zone']   = 'Földrajzi zóna:';
$_['entry_status']     = 'Státusz:';
$_['entry_sort_order'] = 'Sorrend:';
// Hibák
$_['error_permission'] = 'Az Webox modul módosításához nincs jogosultsága!';$_['error_no_cost']    = 'Nincs megadva érvényes szállítási költség! (Csak szám lehet)';

?>
