<?php
class ControllerSaleweboxOrder extends Controller
{
    private $error = array();

    public function index()
    {
        $this->language->load('sale/order');
        $this->language->load('sale/webox_order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/webox_order');

        $this->getList();
    }


    public function download()
    {
	//	echo '<pre>'; print_r($this->request->post); echo '</pre>';
		$this->request->post['webox_name']; //Címzett neve
		$this->request->post['webox_email']; //Címzett e-mail címe
		$this->request->post['webox_phone']; //Címzett telefonszáma
		$this->request->post['webox_mashine']; //Címzett terminál
		$this->request->post['webox_size']; //Csomagméret
		$this->request->post['webox_total']; //Utánvét Összeg
		
		/*
		 * EXAMPLES
Címzett neve;Címzett e-mail címe;Címzett telefonszáma;Címzett terminál;Csomag méret;Utánvét összeg
Teszt Címzett;teszt@example.com;301234567;HUWBX007;A;0
Teszt Címzett2;teszt2@example.com;201234567;HUWBX001;B;3500
		* */
		
		if ($this->request->post['selected'])
		{

	        $this->load->model('sale/order');
	        $this->load->model('sale/webox_order');

			foreach ($this->request->post['selected'] as $order_id)
			{

		        $order_info = $this->model_sale_order->getOrder($order_id);
		        				
				$row  = $order_info['shipping_lastname'].' '.$order_info['shipping_firstname'].";";
				$row .= $order_info['email'].";";
				$row .= $this->model_sale_webox_order->phone_format($order_info['telephone']).";";
				$row .= $this->request->post['webox_mashine'][$order_id].";";
				$row .= $this->request->post['webox_size'][$order_id].";";
				$row .= $this->request->post['webox_total'][$order_id]."";
				$rows[] = $row;
			}
		
		if (count($rows) > 100)
		{
			exit("egyszerre max 100-at lehet");
		}
		
		if (count($rows) > 0)
		{
		
	ob_clean();
	ob_flush();
	header('Content-Type: application/csv');
	header('Content-Disposition: attachment;filename="webox_'.date("Y_m_d").'.csv"');
	header('Cache-Control: max-age=0');
			$content = "Címzett neve;Címzett e-mail címe;Címzett telefonszáma;Címzett terminál;Csomag méret;Utánvét összeg"."\n";
			$content.=implode("\n",$rows);
			
		$content = str_replace("\xC5\x91","\xC3\xB5",$content); // ő
        $content = str_replace("\xC5\xB1","\xC3\xBB",$content); // ű
       
        $content = str_replace("\xC5\x90","\xC3\x95",$content); // Ő
        $content = str_replace("\xC5\xB0","\xC3\x9B",$content); // Ű
			$content = utf8_decode($content);
        
//			echo $content;
			echo $content;
	ob_flush();
	flush();
	exit;
			
		}
			
		}
		else
		{
			exit ("nincs mit exportálni");
		}
		
    }

    protected function getList()
    {
        if (isset($this->request->get['filter_order_id'])) {
            $filter_order_id = $this->request->get['filter_order_id'];
        } else {
            $filter_order_id = null;
        }

        if (isset($this->request->get['filter_order_status_id'])) {
            $filter_order_status_id = $this->request->get['filter_order_status_id'];
        } else {
            $filter_order_status_id = $this->config->get('config_order_status_id');
        }
        
        if (isset($this->request->get['filter_date_added'])) {
            $filter_date_added = $this->request->get['filter_date_added'];
        } else {
            $filter_date_added = null;
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $filter_date_modified = $this->request->get['filter_date_modified'];
        } else {
            $filter_date_modified = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'o.order_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }

        if (isset($this->request->get['filter_order_status_id'])) {
            $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/webox_order', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['download'] = $this->url->link('sale/webox_order/download', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->data['orders'] = array();

        $data = array(
            'filter_order_id' => $filter_order_id,
            'filter_order_status_id' => $filter_order_status_id,
            'filter_date_added' => $filter_date_added,
            'filter_date_modified' => $filter_date_modified,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit')
        );

        $order_total = $this->model_sale_webox_order->getTotalOrders($data);

        $results = $this->model_sale_webox_order->getOrders($data);

        foreach ($results as $result) {
            $action = array();

            $action[] = array(
                'text' => $this->language->get('text_view'),
                'href' => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, 'SSL')
            );

            if (strtotime($result['date_added']) > strtotime('-' . (int)$this->config->get('config_order_edit') . ' day')) {
                $action[] = array(
                    'text' => $this->language->get('text_edit'),
                    'href' => $this->url->link('sale/order/update', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, 'SSL')
                );
            }

            $this->data['orders'][] = array(
                'order_id' => $result['order_id'],
                'customer' => $result['customer'],
                'customer_email' => $result['customer_email'],
                'customer_phone' => $result['customer_phone'],
                'webox_mashine' => $result['webox_mashine'],
                'status' => $result['status'],
                'total' => $result['total'],
                'total_formatted' => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),

                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'date_modified' => date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
                'action' => $action
            );
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['text_missing'] = $this->language->get('text_missing');

        $this->data['column_order_id'] = $this->language->get('column_order_id');
        $this->data['column_parcel_id'] = $this->language->get('column_parcel_id');
        $this->data['column_parcel_size'] = $this->language->get('column_parcel_size');
        $this->data['column_status'] = $this->language->get('column_status');
        $this->data['column_machine_id'] = $this->language->get('column_machine_id');
        $this->data['column_date_added'] = $this->language->get('column_date_added');
        $this->data['column_date_modified'] = $this->language->get('column_date_modified');
        $this->data['column_action'] = $this->language->get('column_action');

        $this->data['column_total'] = $this->language->get('column_total');

        $this->data['button_download'] = $this->language->get('button_download');
        $this->data['button_filter'] = $this->language->get('button_filter');
        $this->data['button_edit'] = $this->language->get('text_edit');

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }

        if (isset($this->request->get['filter_parcel_id'])) {
            $url .= '&filter_parcel_id=' . urlencode(html_entity_decode($this->request->get['filter_parcel_id'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_parcel_size'])) {
            $url .= '&filter_parcel_size=' . $this->request->get['filter_parcel_size'];
        }

        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_machine_id'])) {
            $url .= '&filter_machine_id=' . $this->request->get['filter_machine_id'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['sort_order'] = $this->url->link('sale/webox_order', 'token=' . $this->session->data['token'] . '&sort=o.order_id' . $url, 'SSL');
        $this->data['sort_status'] = $this->url->link('sale/webox_order', 'token=' . $this->session->data['token'] . '&sort=o.webox_status' . $url, 'SSL');
        $this->data['sort_total'] = $this->url->link('sale/webox_order', 'token=' . $this->session->data['token'] . '&sort=o.shipping_address_1' . $url, 'SSL');
        $this->data['sort_date_added'] = $this->url->link('sale/webox_order', 'token=' . $this->session->data['token'] . '&sort=o.date_added' . $url, 'SSL');
        $this->data['sort_date_modified'] = $this->url->link('sale/webox_order', 'token=' . $this->session->data['token'] . '&sort=o.date_modified' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }
        if (isset($this->request->get['filter_order_status_id'])) {
            $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
        }

        if (isset($this->request->get['filter_machine_id'])) {
            $url .= '&filter_machine_id=' . $this->request->get['filter_machine_id'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->data['filter_order_id'] = $filter_order_id;
        $this->data['filter_order_status_id'] = $filter_order_status_id;
        $this->data['filter_date_added'] = $filter_date_added;
        $this->data['filter_date_modified'] = $filter_date_modified;

        $this->load->model('localisation/order_status');
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		$this->data['webox_sizes'] = $this->model_sale_webox_order->getSizes();
        $this->data['sort'] = $sort;
        $this->data['order'] = $order;

        $this->template = 'sale/webox_order_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }
}
?>
